<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
if(env('TURN_VIDEO') == 1){
  Route::any('{query}', function() {

  return view('revolt.live-streaming');

  }) ->where('query', '.*');
}






Route::get('/', function () {
  $meta = [
    'title' => "Book Now India's 1st AI Enabled Electric Motorbike by Revolt Motors",
    'description' => "Book Now India's 1st AI-enabled smart electric bike with next-gen computing & mobility solution from Revolt Motors. Get your high-performance bike with Revolt."
  ];
  return view('revolt.index',['meta' => $meta]);
});

Route::get('/privacy-policy', function () {
  $meta = [
    'title' => "Privacy Policy, Privacy & Terms - Revolt Motors",
    'description' => "Revolt Motors privacy policy has been compiled to better serve the visitors with how & what information we collect & how is being used online."
  ];
  return view('revolt.privacy-policy',['meta' => $meta]);
});
Route::get('/myrevolt-app-privacy-policy', function () {
    return view('revolt.myrevolt-app-privacy-policy');
});

Route::get('/amazon-terms', function () {
    return view('revolt.amazon-terms');
});

Route::get('/from-amazon', function () {
  return view('revolt.from-amazon');
});

// Route::get('/live', function () {
//   return view('revolt.live-streaming');
// });

Route::get('/about-us', function () {
  return view('revolt.about-us');
});

Route::get('/center-locator', function () {
  return view('revolt.center-locator');
});

Route::get('/myrevolt-eula', function () {
  return view('revolt.myrevolt-eula');
});


Route::get('/myrevolt-termsofuse', function () {
  return view('revolt.myrevolt-termsofuse');
});

/*Route::get('/contact-us', function () {
  return view('revolt.contact-us');
});*/


Route::get('/contact-us', 'Users@contactus');

Route::get('/rv400', 'Users@rv400');
Route::get('/rv300', 'Users@rv300');

Route::get('/rv-first', function () {
  $meta = [
    'title' => "RV-First to Join the Revolt Motors Instantly",
    'description' => "Skip the queue and join the Revolt Motors Instantly with RV-First. Get priority support, interact with the product team & personalized rewards from Revolt Motors."
  ];
  return view('revolt.rv-first',['meta' => $meta]);
});

Route::get('thankyou', function () {
    return redirect('/book');
});

// terms & condition
Route::get('/terms', function () {
  $meta = [
    'title' => "Revolt Motors Terms and Conditions | Terms Declaration",
    'description' => "Check the terms & conditions of Revolt Intellicorp Private Limited to understand the terms related to electric bike booking, payment, cancellation & refunds."
  ];
    return view('revolt.terms-condition',['meta' => $meta]);
});

Route::get('/our-story', function () {
    return view('revolt.our-story');
});

Route::get('/faq', function () {
    return view('revolt.faq');
});
Route::get('/charging', function () {
  return view('revolt.charging');
});
Route::get('/press', function () {
  return view('revolt.press');
});
Route::get('/interest','Search@getToptenCity');

Route::get('/faq', function () {
    return view('revolt.faq');
});
Route::get('/googleactiontermsofuse.html', function () {
  return view('revolt.googleactiontermsofuse');
});

Route::get('/product-list', function () {
    return view('revolt.product-list');
});
Route::get('/product-detail', function () {
    return view('revolt.product-detail');
});

Route::get('/cancellation-policy', function () {
    return view('revolt.cancellation-policy');
});

Route::get('/external-order', function () {
    return view('errors.404');
});

Route::get('/our-story', function () {
  return view('revolt.our-story');
});
//toolkit Route
Route::prefix('toolkit')->group(function () {
  Route::get('', 'Toolkit@index');
  Route::get('motorcycles', 'Toolkit@motorcycles');
  Route::get('whyelectric', 'Toolkit@whyElectric');
  Route::get('rv400', 'Toolkit@rv400');
  Route::get('rv300', 'Toolkit@rv300');
  Route::post('sendQuote','Toolkit@sendQuote');
});
Route::post('add-user','Users@addUser');
Route::post('send-email','Users@sendEmail');
Route::get('autocomplete','Search@search');
Route::post('add-booking-user','Users@addBookUser');
Route::post('send-booking-email','Users@SendBookingEmail');
Route::post('check-mobile-exist','Users@CheckMobileExist');
Route::post('create_order','Users@createOrder');
Route::post('add-bike-color','Users@addBikeColor');
Route::post('thankyou','Users@orderSuccess');
Route::post('success','Users@externalPayment');
Route::get('booking-leads','Users@getLeads');
Route::get('home-leads','Users@getHomeLeads');
Route::get('bulk_upload/', 'WelcomeController@bulk_upload');
Route::get('send-mail-failed-order/', 'Users@sendMailFailedOrder');
Route::post('check-coupon/', 'Users@checkCouponExist');
Route::post('external-order/', 'Users@externalOrder');
//Route::get('pending-payment/{userid}', 'Users@pendingPayment');
Route::post('save-coupon/', 'Users@saveUserCoupon'); // save coupon from pending payment page
//Route::get('payment-awaiting/{is_testing?}', 'Users@paymentAwaitedUsers');
Route::post('subscrption', 'Users@addSubscrption');
Route::post('get-dealer', 'Users@FetchDealerList');
Route::get('book/{product?}/{color?}', 'Users@prebook'); // booking page
Route::get('dealer-info', 'Users@dealerInfo'); // dealer info page
Route::post('update-payment', 'Users@updatePaymentMode'); // updatepayment mode
//Route::get('loan-status', 'Users@checkLoanStatus'); // check loan status
Route::get('update-customer-info-with-batch/', 'Users@updateCityDeliveryBatch'); // update customer info,delivery batch,send revised mailer

//Route::resource('images', 'WelcomeController', ['only' => ['store', 'destroy']]);
Route::get('/foundry-profile',function(){
  return view('revolt.foundry');
});

Route::get('/cancel-my-revolt', 'Users@CancelBooking'); // cancel booking

Route::get('test-ride/', 'Users@rideTest');
Route::get('unlock-city/', 'Users@unlockCity');
Route::post('get-area/', 'Users@fetchAreaList');
Route::post('get-city/', 'Users@fetchCityList');
Route::post('get-region/', 'Users@fetchRegionList');
Route::post('add-test-drive/', 'Users@saveTestDrive');
Route::get('loan-status/', 'Users@loan_view');
Route::post('check-loan-status/', 'Users@checkLoanStatus');
Route::post('cancel-my-revolt/', 'Users@cancelBooking');
Route::post('check-mobile/', 'Users@checkMobile');
Route::post('add-contact/', 'ContactUs@addContact');
Route::post('allDealers', 'Users@allDealers');
Route::post('testRideSlots', 'Users@assignedSlots');
Route::post('checkSlotsAvailablity', 'Users@checkSlotsAvailablity');
Route::post('get-slot', 'Users@slot');
Route::get('test-ride-entry/', 'Users@setGarbageEntry');
Route::post('booking-update/', 'Users@CancelUserBooking');
Route::post('prebook-cancel/', 'Users@cancelPrebooking');
Route::post('verify-otp/', 'Users@verifyOtp');
Route::post('verify-otp-prebooking/', 'Users@verifyOtpPrebook');
Route::post('resend-otp/', 'Users@resendOtp');
Route::post('save-reason/', 'Users@addReason');
Route::post('send-cancel-email/', 'Users@sendMailToCancelUser');
Route::post('send-cancel-email-prebook/', 'Users@sendMailToPrebookUser');
Route::post('save-unlock/', 'Users@addUnlock');
Route::get('revoltinyourcity/', 'ReserveSpots@index'); // Resource Spot
Route::post('create-resource-spot/', 'ReserveSpots@save_lead'); // save Resource Spot

